﻿using System.Collections.Generic;

namespace HUCE.Book.Authentication.External
{
    public interface IExternalAuthConfiguration
    {
        List<ExternalLoginProviderInfo> Providers { get; }
    }
}
