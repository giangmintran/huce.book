﻿using Abp.AutoMapper;
using HUCE.Book.Authentication.External;

namespace HUCE.Book.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
