﻿using Abp.MultiTenancy;
using HUCE.Book.Authorization.Users;

namespace HUCE.Book.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
