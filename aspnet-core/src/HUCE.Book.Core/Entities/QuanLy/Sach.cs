﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book.Entities
{
    public class Sach : Entity<int>, IFullAudited
    {
        public string Ma { get; set; }
        public string Ten { get; set; }
        public string TenEn { get; set; }
        public string NhaXuatBan { get; set; }
        public string TacGia { get; set; }
        public int? SoLuong { get; set; }
        public long? Gia { get; set; }
        public string MoTa { get; set; }
        public int NhaPhanPhoiId { get; set; }
        public int TheLoaiSachId { get; set; }
        public int KhoId { get; set; }

        public long? CreatorUserId { get; set; }
        public DateTime CreationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public bool IsDeleted { get; set; }
    }
}
