﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book.Entities
{
    public class DonHang : Entity<int>, IFullAudited
    {
        public int? GioHangId { get; set; }
        public string Ma { get; set; }
        public int? VoucherId { get; set; }
        public string Sdt { get; set; }
        public string DiaChi { get; set; }
        public string TinNhan { get; set; }
        public long? ThanhTien { get; set; }
        public bool IsDuyet { get; set; } 
        public string PhuongThucVanChuyen { get; set; }
        public string PhuongThucThanhToan { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime CreationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public bool IsDeleted { get; set; }
    }

   /* public class ChiTietDonHangs : Entity<int>, ISoftDelete
    {
        public int DonHangId { get; set; }
        public int? SachId { get; set; }
        public int? SoLuong { get; set; }
        public string TrangThai { get; set; }
        #region Trạng thái đơn giao
        /// <summary>
        /// Trạng thái đang giao hàng
        /// </summary>
        public const string DANG_GIAO_HANG = "DANG_GIAO_HANG";
        /// <summary>
        /// Trạng thái đã nhận hàng
        /// </summary>
        public const string DA_NHAN_HANG = "DA_NHAN_HANG";
        /// <summary>
        /// Trạng thái đang chuẩn bị hàng
        /// </summary>
        public const string DANG_CHUAN_BI_HANG = "DANG_CHUAN_BI_HANG";
        /// <summary>
        /// Trạng thái chưa xử lý
        /// </summary>
        public const string CHUA_XU_LY = "CHUA_XU_LY";
        #endregion
        public bool IsDeleted { get; set; }
    }*/
}
