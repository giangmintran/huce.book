﻿using HUCE.Book.Debugging;

namespace HUCE.Book
{
    public class BookConsts
    {
        public const string LocalizationSourceName = "Book";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;


        /// <summary>
        /// Default pass phrase for SimpleStringCipher decrypt/encrypt operations
        /// </summary>
        public static readonly string DefaultPassPhrase =
            DebugHelper.IsDebug ? "gsKxGZ012HLL3MI5" : "369a942ab59d40a0b1845a3ef264fcfc";
    }
}
