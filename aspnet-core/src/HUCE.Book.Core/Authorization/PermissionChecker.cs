﻿using Abp.Authorization;
using HUCE.Book.Authorization.Roles;
using HUCE.Book.Authorization.Users;

namespace HUCE.Book.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
