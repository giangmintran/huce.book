﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace HUCE.Book.Authorization
{
    public class BookAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            context.CreatePermission(PermissionNames.Pages_Users, L("Users"));
            context.CreatePermission(PermissionNames.Pages_Users_Activation, L("UsersActivation"));
            context.CreatePermission(PermissionNames.Pages_Roles, L("Roles"));
            context.CreatePermission(PermissionNames.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);

            context.CreatePermission(PermissionNames.Pages_NhaPhanPhoi, L("PagesNhaPhanPhoi"));
            context.CreatePermission(PermissionNames.Actions_NhaPhanPhoi_Create, L("Create"));
            context.CreatePermission(PermissionNames.Actions_NhaPhanPhoi_Update, L("AUpdate"));
            context.CreatePermission(PermissionNames.Actions_NhaPhanPhoi_Delete, L("Delete"));

            context.CreatePermission(PermissionNames.Pages_PhuongThucThanhToan, L("PagesPhuongThucThanhToan"));
            context.CreatePermission(PermissionNames.Actions_PhuongThucThanhToan_Create, L("Create"));
            context.CreatePermission(PermissionNames.Actions_PhuongThucThanhToan_Update, L("AUpdate"));
            context.CreatePermission(PermissionNames.Actions_PhuongThucThanhToan_Delete, L("Delete"));

            context.CreatePermission(PermissionNames.Pages_Kho, L("PagesKho"));
            context.CreatePermission(PermissionNames.Actions_Kho_Create, L("Create"));
            context.CreatePermission(PermissionNames.Actions_Kho_Update, L("AUpdate"));
            context.CreatePermission(PermissionNames.Actions_Kho_Delete, L("Delete"));

            context.CreatePermission(PermissionNames.Pages_TheLoaiSach, L("PagesTheLoaiSach"));
            context.CreatePermission(PermissionNames.Actions_TheLoaiSach_Create, L("Create"));
            context.CreatePermission(PermissionNames.Actions_TheLoaiSach_Update, L("AUpdate"));
            context.CreatePermission(PermissionNames.Actions_TheLoaiSach_Delete, L("Delete"));

            context.CreatePermission(PermissionNames.Pages_Voucher, L("PagesVoucher"));
            context.CreatePermission(PermissionNames.Actions_Voucher_Create, L("Create"));
            context.CreatePermission(PermissionNames.Actions_Voucher_Update, L("AUpdate"));
            context.CreatePermission(PermissionNames.Actions_Voucher_Delete, L("Delete"));

            context.CreatePermission(PermissionNames.Pages_QuanLySach, L("PagesQuanLySach"));
            context.CreatePermission(PermissionNames.Actions_QuanLySach_Create, L("Create"));
            context.CreatePermission(PermissionNames.Actions_QuanLySach_Update, L("AUpdate"));
            context.CreatePermission(PermissionNames.Actions_QuanLySach_Delete, L("Delete"));

            context.CreatePermission(PermissionNames.Pages_GioHang, L("PagesGioHang"));
            context.CreatePermission(PermissionNames.Actions_GioHang_Create, L("Create"));
            context.CreatePermission(PermissionNames.Actions_GioHang_Update, L("AUpdate"));
            context.CreatePermission(PermissionNames.Actions_GioHang_Delete, L("Delete"));

            context.CreatePermission(PermissionNames.Pages_DonHang, L("PagesDonHang"));
            context.CreatePermission(PermissionNames.Actions_DonHang_Create, L("Create"));
            context.CreatePermission(PermissionNames.Actions_DonHang_Update, L("AUpdate"));
            context.CreatePermission(PermissionNames.Actions_DonHang_Delete, L("Delete"));
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, BookConsts.LocalizationSourceName);
        }
    }
}
