﻿namespace HUCE.Book.Authorization
{
    public static class PermissionNames
    {
        public const string Pages_Tenants = "Pages.Tenants";

        public const string Pages_Users = "Pages.Users";
        public const string Pages_Users_Activation = "Pages.Users.Activation";
        public const string Pages_Roles = "Pages.Roles";

        #region Cấu hình
        public const string Pages_NhaPhanPhoi = "Pages.NhaPhanPhoi";
        public const string Actions_NhaPhanPhoi_Create = "Actions.NhaPhanPhoi.Create";
        public const string Actions_NhaPhanPhoi_Update = "Actions.NhaPhanPhoi.Update";
        public const string Actions_NhaPhanPhoi_Delete = "Actions.NhaPhanPhoi.Delete";

        public const string Pages_TheLoaiSach = "Pages.TheLoaiSach";
        public const string Actions_TheLoaiSach_Create = "Actions.TheLoaiSach.Create";
        public const string Actions_TheLoaiSach_Update = "Actions.TheLoaiSach.Update";
        public const string Actions_TheLoaiSach_Delete = "Actions.TheLoaiSach.Delete";

        public const string Pages_Kho = "Pages.Kho";
        public const string Actions_Kho_Create = "Actions.Kho.Create";
        public const string Actions_Kho_Update = "Actions.Kho.Update";
        public const string Actions_Kho_Delete = "Actions.Kho.Delete";

        public const string Pages_PhuongThucThanhToan = "Pages.PhuongThucThanhToan";
        public const string Actions_PhuongThucThanhToan_Create = "Actions.PhuongThucThanhToan.Create";
        public const string Actions_PhuongThucThanhToan_Update = "Actions.PhuongThucThanhToan.Update";
        public const string Actions_PhuongThucThanhToan_Delete = "Actions.PhuongThucThanhToan.Delete";
        
        public const string Pages_Voucher = "Pages.Voucher";
        public const string Actions_Voucher_Create = "Actions.Voucher.Create";
        public const string Actions_Voucher_Update = "Actions.Voucher.Update";
        public const string Actions_Voucher_Delete = "Actions.Voucher.Delete";
        #endregion

        #region
        public const string Pages_QuanLySach = "Pages.QuanLySach";
        public const string Actions_QuanLySach_Create = "Actions.QuanLySach.Create";
        public const string Actions_QuanLySach_Update = "Actions.QuanLySach.Update";
        public const string Actions_QuanLySach_Delete = "Actions.QuanLySach.Delete";

        public const string Pages_GioHang = "Pages.GioHang";
        public const string Actions_GioHang_Create = "Actions.GioHang.Create";
        public const string Actions_GioHang_Update = "Actions.GioHang.Update";
        public const string Actions_GioHang_Delete = "Actions.GioHang.Delete";

        public const string Pages_DonHang = "Pages.DonHang";
        public const string Actions_DonHang_Create = "Actions.DonHang.Create";
        public const string Actions_DonHang_Update = "Actions.DonHang.Update";
        public const string Actions_DonHang_Delete = "Actions.DonHang.Delete";
        #endregion
    }
}
