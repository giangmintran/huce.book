﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using HUCE.Book.Authorization.Roles;
using HUCE.Book.Authorization.Users;
using HUCE.Book.MultiTenancy;
using HUCE.Book.Entities;

namespace HUCE.Book.EntityFrameworkCore
{
    public class BookDbContext : AbpZeroDbContext<Tenant, Role, User, BookDbContext>
    {
        /* Define a DbSet for each entity of the application */
        
        public BookDbContext(DbContextOptions<BookDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region Cấu hình
            modelBuilder.Entity<NhaPhanPhoi>().HasKey(entity => entity.Id);
            modelBuilder.Entity<NhaPhanPhoi>(entity =>
            {
                entity.ToTable("BOOK_NHA_PHAN_PHOI");

                entity.Property(e => e.Id)
                    .HasColumnName("Id")
                    .ValueGeneratedOnAdd()
                    .IsRequired();
                entity.Property(e => e.Ma)
                    .HasColumnType("varchar(15)")
                    .IsRequired();
                entity.Property(e => e.Ten)
                    .HasColumnType("nvarchar(500)")
                    .IsRequired();

                entity.Property(e => e.DiaChi)
                    .HasColumnType("nvarchar(500)");

                entity.Property(e => e.Sdt)
                    .HasColumnType("varchar(13)");

                entity.Property(e => e.MoTa)
                    .HasColumnType("nvarchar(500)");

                entity.Property(e => e.Email)
                    .HasColumnType("nvarchar(500)");

                entity.Property(e => e.IsDeleted)
                    .HasColumnName("IsDeleted")
                    .IsRequired();
            });

            modelBuilder.Entity<TheLoaiSach>().HasKey(entity => entity.Id);
            modelBuilder.Entity<TheLoaiSach>(entity =>
            {
                entity.ToTable("BOOK_THE_LOAI_SACH");

                entity.Property(e => e.Id)
                    .HasColumnName("Id")
                    .ValueGeneratedOnAdd()
                    .IsRequired();

                entity.Property(e => e.Ten)
                    .HasColumnType("nvarchar(500)")
                    .IsRequired();

                entity.Property(e => e.MoTa)
                    .HasColumnType("nvarchar(500)");

                entity.Property(e => e.IsDeleted)
                    .HasColumnName("IsDeleted")
                    .IsRequired();
            });

            modelBuilder.Entity<PhuongThucThanhToan>().HasKey(entity => entity.Id);
            modelBuilder.Entity<PhuongThucThanhToan>(entity =>
            {
                entity.ToTable("BOOK_PHUONG_THUC_THANH_TOAN");

                entity.Property(e => e.Id)
                    .HasColumnName("Id")
                    .ValueGeneratedOnAdd()
                    .IsRequired();

                entity.Property(e => e.Ten)
                    .HasColumnType("nvarchar(500)")
                    .IsRequired();

                entity.Property(e => e.MoTa)
                    .HasColumnType("nvarchar(500)");

                entity.Property(e => e.IsDeleted)
                    .HasColumnName("IsDeleted")
                    .IsRequired();
            });
            modelBuilder.Entity<PhuongThucThanhToan>()
                .HasIndex(e => new { e.IsDeleted })
                .HasFilter("IsDeleted <> 1")
                .IsUnique();

            modelBuilder.Entity<Voucher>().HasKey(entity => entity.Id);
            modelBuilder.Entity<Voucher>(entity =>
            {
                entity.ToTable("BOOK_VOUCHER");

                entity.Property(e => e.Id)
                    .HasColumnName("Id")
                    .ValueGeneratedOnAdd()
                    .IsRequired();

                entity.Property(e => e.MaVoucher)
                    .HasColumnType("varchar(10)")
                    .IsRequired();

                entity.Property(e => e.Ten)
                    .HasColumnType("nvarchar(500)")
                    .IsRequired();

                entity.Property(e => e.LoaiVoucher)
                    .HasColumnType("int");

                entity.Property(e => e.Gia)
                   .HasColumnType("int")
                   .IsRequired();

                entity.Property(e => e.MoTa)
                    .HasColumnType("nvarchar(500)");

                entity.Property(e => e.NgayBatDau)
                    .HasColumnType("Date");

                entity.Property(e => e.NgayKetThuc)
                    .HasColumnType("Date");

                entity.Property(e => e.IsDeleted)
                    .HasColumnName("IsDeleted")
                    .IsRequired();
            });

            modelBuilder.Entity<Kho>().HasKey(entity => entity.Id);
            modelBuilder.Entity<Kho>(entity =>
            {
                entity.ToTable("BOOK_KHO");

                entity.Property(e => e.Id)
                    .HasColumnName("Id")
                    .ValueGeneratedOnAdd()
                    .IsRequired();
                entity.Property(e => e.Ma)
                    .HasColumnType("nvarchar(15)");

                entity.Property(e => e.Ten)
                    .HasColumnType("nvarchar(500)")
                    .IsRequired();

                entity.Property(e => e.SoLuong)
                    .HasColumnType("int");

                entity.Property(e => e.DiaChi)
                    .HasColumnType("nvarchar(500)");

                entity.Property(e => e.IsDeleted)
                    .HasColumnName("IsDeleted")
                    .IsRequired();
            });
            #endregion

            #region Quản lý Sách
            modelBuilder.Entity<Sach>().HasKey(entity => entity.Id);
            modelBuilder.Entity<Sach>(entity =>
            {
                entity.ToTable("BOOK_SACH");

                entity.Property(e => e.Id)
                    .HasColumnName("Id")
                    .ValueGeneratedOnAdd()
                    .IsRequired();

                entity.Property(e => e.Ten)
                    .HasColumnType("nvarchar(100)")
                    .IsRequired();

                entity.Property(e => e.TenEn)
                    .HasColumnType("nvarchar(100)");

                entity.Property(e => e.NhaXuatBan)
                    .HasColumnType("nvarchar(200)")
                    .IsRequired();

                entity.Property(e => e.TacGia)
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.SoLuong)
                    .HasColumnType("int");

                entity.Property(e => e.Gia)
                    .HasColumnType("int");

                entity.Property(e => e.MoTa)
                    .HasColumnType("nvarchar(500)");

                entity.Property(e => e.NhaPhanPhoiId)
                    .HasColumnType("int");

                entity.Property(e => e.TheLoaiSachId)
                    .HasColumnType("int");

                entity.Property(e => e.KhoId)
                    .HasColumnType("int");

                entity.Property(e => e.MoTa)
                    .HasColumnType("nvarchar(500)");

                entity.Property(e => e.IsDeleted)
                    .HasColumnName("IsDeleted")
                    .IsRequired();
            });
            modelBuilder.Entity<Sach>().HasIndex(entity => entity.TheLoaiSachId);
            modelBuilder.Entity<Sach>().HasIndex(entity => entity.KhoId);
            modelBuilder.Entity<Sach>().HasIndex(entity => entity.NhaPhanPhoiId);

            modelBuilder.Entity<DonHang>().HasKey(entity => entity.Id);
            modelBuilder.Entity<DonHang>(entity =>
            {
                entity.ToTable("BOOK_DON_HANG");

                entity.Property(e => e.Id)
                    .HasColumnName("Id")
                    .ValueGeneratedOnAdd()
                    .IsRequired();

                entity.Property(e => e.Ma)
                    .HasColumnType("varchar(10)")
                    .IsRequired();

                entity.Property(e => e.Sdt)
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.DiaChi)
                    .HasColumnType("nvarchar(500)")
                    .IsRequired();

                entity.Property(e => e.TinNhan)
                    .HasColumnType("nvarchar(4000)");

                entity.Property(e => e.ThanhTien)
                    .HasColumnType("BIGINT");

                entity.Property(e => e.IsDuyet)
                    .HasColumnType("bit")
                    .HasDefaultValue(false);

                entity.Property(e => e.PhuongThucThanhToan)
                    .HasColumnType("nvarchar(100)")
                    .IsRequired();

                entity.Property(e => e.PhuongThucVanChuyen)
                    .HasColumnType("nvarchar(100)")
                    .IsRequired();

                entity.Property(e => e.VoucherId)
                    .HasColumnType("int");

                entity.Property(e => e.IsDeleted)
                    .HasColumnName("IsDeleted")
                    .IsRequired();
            });
            modelBuilder.Entity<DonHang>().HasIndex(entity => entity.VoucherId);
            modelBuilder.Entity<DonHang>().HasIndex(entity => entity.GioHangId);

            /* modelBuilder.Entity<ChiTietDonHangs>().HasKey(entity => entity.Id);
             modelBuilder.Entity<ChiTietDonHangs>(entity =>
             {
                 entity.ToTable("BOOK_CHI_TIET_DON_HANG");

                 entity.Property(e => e.Id)
                     .HasColumnName("Id")
                     .ValueGeneratedOnAdd()
                     .IsRequired();

                 entity.Property(e => e.DonHangId)
                     .HasColumnType("int")
                     .IsRequired();

                 entity.Property(e => e.SoLuong)
                     .HasColumnType("int");

                 entity.Property(e => e.TrangThai)
                     .HasColumnType("nvarchar(50)");

             });
             modelBuilder.Entity<ChiTietDonHangs>().HasIndex(entity => entity.DonHangId);
            */

            modelBuilder.Entity<GioHang>().HasKey(entity => entity.Id);
            modelBuilder.Entity<GioHang>(entity =>
            {
                entity.ToTable("BOOK_GIO_HANG");

                entity.Property(e => e.Id)
                    .HasColumnName("Id")
                    .ValueGeneratedOnAdd()
                    .IsRequired();

                entity.Property(e => e.IsDeleted)
                    .HasColumnName("IsDeleted")
                    .IsRequired();
            });

            modelBuilder.Entity<ChiTietGioHang>().HasKey(entity => entity.Id);
            modelBuilder.Entity<ChiTietGioHang>(entity =>
            {
                entity.ToTable("BOOK_CHI_TIET_GIO_HANG");

                entity.Property(e => e.Id)
                    .HasColumnName("Id")
                    .ValueGeneratedOnAdd()
                    .IsRequired();

                entity.Property(e => e.GioHangId)
                    .HasColumnName("GioHangId")
                    .HasColumnType("int")
                    .IsRequired();

                entity.Property(e => e.SachId)
                    .HasColumnName("SachId")
                    .HasColumnType("int")
                    .IsRequired();

                entity.Property(e => e.SoLuong)
                    .HasColumnType("int")
                    .IsRequired();

                entity.Property(e => e.Gia)
                    .IsRequired();

                entity.Property(e => e.IsDeleted)
                    .HasColumnName("IsDeleted")
                    .IsRequired();
            });
            modelBuilder.Entity<ChiTietGioHang>().HasIndex(entity => entity.GioHangId);
            #endregion
        }
    }
}
