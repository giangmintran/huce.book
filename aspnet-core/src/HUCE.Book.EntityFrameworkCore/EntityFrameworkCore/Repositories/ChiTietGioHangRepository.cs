﻿using Abp.EntityFrameworkCore;
using HUCE.Book.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book.EntityFrameworkCore.Repositories
{
    public class ChiTietGioHangRepository : BookRepositoryBase<ChiTietGioHang>
    {
        public ChiTietGioHangRepository(IDbContextProvider<BookDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
