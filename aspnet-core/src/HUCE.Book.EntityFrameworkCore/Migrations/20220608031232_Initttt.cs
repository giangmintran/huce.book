﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HUCE.Book.Migrations
{
    public partial class Initttt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BOOK_CHI_TIET_DON_HANG");

            migrationBuilder.DropIndex(
                name: "IX_BOOK_VOUCHER_IsDeleted",
                table: "BOOK_VOUCHER");

            migrationBuilder.DropIndex(
                name: "IX_BOOK_THE_LOAI_SACH_IsDeleted",
                table: "BOOK_THE_LOAI_SACH");

            migrationBuilder.DropIndex(
                name: "IX_BOOK_SACH_Ma_IsDeleted",
                table: "BOOK_SACH");

            migrationBuilder.DropIndex(
                name: "IX_BOOK_NHA_PHAN_PHOI_IsDeleted",
                table: "BOOK_NHA_PHAN_PHOI");

            migrationBuilder.AlterColumn<string>(
                name: "Ma",
                table: "BOOK_SACH",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PhuongThucVanChuyen",
                table: "BOOK_DON_HANG",
                type: "nvarchar(100)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PhuongThucThanhToan",
                table: "BOOK_DON_HANG",
                type: "nvarchar(100)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "GioHangId",
                table: "BOOK_DON_HANG",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_CHI_TIET_GIO_HANG_GioHangId",
                table: "BOOK_CHI_TIET_GIO_HANG",
                column: "GioHangId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_BOOK_CHI_TIET_GIO_HANG_GioHangId",
                table: "BOOK_CHI_TIET_GIO_HANG");

            migrationBuilder.DropColumn(
                name: "GioHangId",
                table: "BOOK_DON_HANG");

            migrationBuilder.AlterColumn<string>(
                name: "Ma",
                table: "BOOK_SACH",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PhuongThucVanChuyen",
                table: "BOOK_DON_HANG",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)");

            migrationBuilder.AlterColumn<string>(
                name: "PhuongThucThanhToan",
                table: "BOOK_DON_HANG",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)");

            migrationBuilder.CreateTable(
                name: "BOOK_CHI_TIET_DON_HANG",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DonHangId = table.Column<int>(type: "int", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    SachId = table.Column<int>(type: "int", nullable: true),
                    SoLuong = table.Column<int>(type: "int", nullable: true),
                    TrangThai = table.Column<string>(type: "nvarchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BOOK_CHI_TIET_DON_HANG", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_VOUCHER_IsDeleted",
                table: "BOOK_VOUCHER",
                column: "IsDeleted",
                unique: true,
                filter: "IsDeleted <> 1");

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_THE_LOAI_SACH_IsDeleted",
                table: "BOOK_THE_LOAI_SACH",
                column: "IsDeleted",
                unique: true,
                filter: "IsDeleted <> 1");

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_SACH_Ma_IsDeleted",
                table: "BOOK_SACH",
                columns: new[] { "Ma", "IsDeleted" },
                unique: true,
                filter: "IsDeleted <> 1");

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_NHA_PHAN_PHOI_IsDeleted",
                table: "BOOK_NHA_PHAN_PHOI",
                column: "IsDeleted",
                unique: true,
                filter: "IsDeleted <> 1");

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_CHI_TIET_DON_HANG_DonHangId",
                table: "BOOK_CHI_TIET_DON_HANG",
                column: "DonHangId");
        }
    }
}
