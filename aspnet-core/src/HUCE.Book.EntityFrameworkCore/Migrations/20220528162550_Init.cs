﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HUCE.Book.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BOOK_CHI_TIET_DON_HANG",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DonHangId = table.Column<int>(type: "int", nullable: false),
                    SachId = table.Column<int>(type: "int", nullable: true),
                    SoLuong = table.Column<int>(type: "int", nullable: true),
                    TrangThai = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BOOK_CHI_TIET_DON_HANG", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BOOK_DON_HANG",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Ma = table.Column<string>(type: "varchar(10)", nullable: false),
                    VoucherId = table.Column<int>(type: "int", nullable: true),
                    Sdt = table.Column<string>(type: "varchar(15)", nullable: true),
                    DiaChi = table.Column<string>(type: "nvarchar(500)", nullable: false),
                    TinNhan = table.Column<string>(type: "nvarchar(4000)", nullable: true),
                    ThanhTien = table.Column<long>(type: "BIGINT", nullable: true),
                    IsDuyet = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    PhuongThucVanChuyen = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhuongThucThanhToan = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BOOK_DON_HANG", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BOOK_KHO",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Ma = table.Column<string>(type: "nvarchar(15)", nullable: true),
                    Ten = table.Column<string>(type: "nvarchar(500)", nullable: false),
                    SoLuong = table.Column<int>(type: "int", nullable: false),
                    DiaChi = table.Column<string>(type: "nvarchar(500)", nullable: true),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BOOK_KHO", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BOOK_NHA_PHAN_PHOI",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Ma = table.Column<string>(type: "varchar(15)", nullable: false),
                    Ten = table.Column<string>(type: "nvarchar(500)", nullable: false),
                    Sdt = table.Column<string>(type: "varchar(13)", nullable: true),
                    DiaChi = table.Column<string>(type: "nvarchar(500)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(500)", nullable: true),
                    MoTa = table.Column<string>(type: "nvarchar(500)", nullable: true),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BOOK_NHA_PHAN_PHOI", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BOOK_PHUONG_THUC_THANH_TOAN",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Ten = table.Column<string>(type: "nvarchar(500)", nullable: false),
                    MoTa = table.Column<string>(type: "nvarchar(500)", nullable: true),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BOOK_PHUONG_THUC_THANH_TOAN", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BOOK_SACH",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Ma = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Ten = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    TenEn = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    NhaXuatBan = table.Column<string>(type: "nvarchar(200)", nullable: false),
                    TacGia = table.Column<string>(type: "varchar(200)", nullable: true),
                    SoLuong = table.Column<int>(type: "int", nullable: true),
                    Gia = table.Column<int>(type: "int", nullable: true),
                    MoTa = table.Column<string>(type: "nvarchar(500)", nullable: true),
                    NhaPhanPhoiId = table.Column<int>(type: "int", nullable: false),
                    TheLoaiSachId = table.Column<int>(type: "int", nullable: false),
                    KhoId = table.Column<int>(type: "int", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BOOK_SACH", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BOOK_THE_LOAI_SACH",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Ten = table.Column<string>(type: "nvarchar(500)", nullable: false),
                    MoTa = table.Column<string>(type: "nvarchar(500)", nullable: true),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BOOK_THE_LOAI_SACH", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BOOK_VOUCHER",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MaVoucher = table.Column<string>(type: "varchar(10)", nullable: false),
                    Ten = table.Column<string>(type: "nvarchar(500)", nullable: false),
                    LoaiVoucher = table.Column<int>(type: "int", nullable: true),
                    Gia = table.Column<int>(type: "int", nullable: false),
                    MoTa = table.Column<string>(type: "nvarchar(500)", nullable: true),
                    NgayBatDau = table.Column<DateTime>(type: "Date", nullable: false),
                    NgayKetThuc = table.Column<DateTime>(type: "Date", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BOOK_VOUCHER", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_CHI_TIET_DON_HANG_DonHangId",
                table: "BOOK_CHI_TIET_DON_HANG",
                column: "DonHangId");

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_DON_HANG_VoucherId",
                table: "BOOK_DON_HANG",
                column: "VoucherId");

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_NHA_PHAN_PHOI_Ten_IsDeleted",
                table: "BOOK_NHA_PHAN_PHOI",
                columns: new[] { "Ten", "IsDeleted" },
                unique: true,
                filter: "IsDeleted <> 1");

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_PHUONG_THUC_THANH_TOAN_Ten_IsDeleted",
                table: "BOOK_PHUONG_THUC_THANH_TOAN",
                columns: new[] { "Ten", "IsDeleted" },
                unique: true,
                filter: "IsDeleted <> 1");

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_SACH_KhoId",
                table: "BOOK_SACH",
                column: "KhoId");

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_SACH_NhaPhanPhoiId",
                table: "BOOK_SACH",
                column: "NhaPhanPhoiId");

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_SACH_Ten_IsDeleted",
                table: "BOOK_SACH",
                columns: new[] { "Ten", "IsDeleted" },
                unique: true,
                filter: "IsDeleted <> 1");

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_SACH_TheLoaiSachId",
                table: "BOOK_SACH",
                column: "TheLoaiSachId");

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_THE_LOAI_SACH_Ten_IsDeleted",
                table: "BOOK_THE_LOAI_SACH",
                columns: new[] { "Ten", "IsDeleted" },
                unique: true,
                filter: "IsDeleted <> 1");

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_VOUCHER_Ten_IsDeleted",
                table: "BOOK_VOUCHER",
                columns: new[] { "Ten", "IsDeleted" },
                unique: true,
                filter: "IsDeleted <> 1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BOOK_CHI_TIET_DON_HANG");

            migrationBuilder.DropTable(
                name: "BOOK_DON_HANG");

            migrationBuilder.DropTable(
                name: "BOOK_KHO");

            migrationBuilder.DropTable(
                name: "BOOK_NHA_PHAN_PHOI");

            migrationBuilder.DropTable(
                name: "BOOK_PHUONG_THUC_THANH_TOAN");

            migrationBuilder.DropTable(
                name: "BOOK_SACH");

            migrationBuilder.DropTable(
                name: "BOOK_THE_LOAI_SACH");

            migrationBuilder.DropTable(
                name: "BOOK_VOUCHER");
        }
    }
}
