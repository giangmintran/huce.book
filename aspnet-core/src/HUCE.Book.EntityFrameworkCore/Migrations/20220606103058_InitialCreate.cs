﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HUCE.Book.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_BOOK_VOUCHER_Ten_IsDeleted",
                table: "BOOK_VOUCHER");

            migrationBuilder.DropIndex(
                name: "IX_BOOK_THE_LOAI_SACH_Ten_IsDeleted",
                table: "BOOK_THE_LOAI_SACH");

            migrationBuilder.DropIndex(
                name: "IX_BOOK_SACH_Ten_IsDeleted",
                table: "BOOK_SACH");

            migrationBuilder.DropIndex(
                name: "IX_BOOK_PHUONG_THUC_THANH_TOAN_Ten_IsDeleted",
                table: "BOOK_PHUONG_THUC_THANH_TOAN");

            migrationBuilder.DropIndex(
                name: "IX_BOOK_NHA_PHAN_PHOI_Ten_IsDeleted",
                table: "BOOK_NHA_PHAN_PHOI");

            migrationBuilder.AlterColumn<string>(
                name: "Ma",
                table: "BOOK_SACH",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "BOOK_CHI_TIET_GIO_HANG",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GioHangId = table.Column<int>(type: "int", nullable: false),
                    SachId = table.Column<int>(type: "int", nullable: false),
                    Gia = table.Column<double>(type: "float", nullable: false),
                    SoLuong = table.Column<int>(type: "int", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BOOK_CHI_TIET_GIO_HANG", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BOOK_GIO_HANG",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BOOK_GIO_HANG", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_VOUCHER_IsDeleted",
                table: "BOOK_VOUCHER",
                column: "IsDeleted",
                unique: true,
                filter: "IsDeleted <> 1");

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_THE_LOAI_SACH_IsDeleted",
                table: "BOOK_THE_LOAI_SACH",
                column: "IsDeleted",
                unique: true,
                filter: "IsDeleted <> 1");

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_SACH_Ma_IsDeleted",
                table: "BOOK_SACH",
                columns: new[] { "Ma", "IsDeleted" },
                unique: true,
                filter: "IsDeleted <> 1");

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_PHUONG_THUC_THANH_TOAN_IsDeleted",
                table: "BOOK_PHUONG_THUC_THANH_TOAN",
                column: "IsDeleted",
                unique: true,
                filter: "IsDeleted <> 1");

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_NHA_PHAN_PHOI_IsDeleted",
                table: "BOOK_NHA_PHAN_PHOI",
                column: "IsDeleted",
                unique: true,
                filter: "IsDeleted <> 1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BOOK_CHI_TIET_GIO_HANG");

            migrationBuilder.DropTable(
                name: "BOOK_GIO_HANG");

            migrationBuilder.DropIndex(
                name: "IX_BOOK_VOUCHER_IsDeleted",
                table: "BOOK_VOUCHER");

            migrationBuilder.DropIndex(
                name: "IX_BOOK_THE_LOAI_SACH_IsDeleted",
                table: "BOOK_THE_LOAI_SACH");

            migrationBuilder.DropIndex(
                name: "IX_BOOK_SACH_Ma_IsDeleted",
                table: "BOOK_SACH");

            migrationBuilder.DropIndex(
                name: "IX_BOOK_PHUONG_THUC_THANH_TOAN_IsDeleted",
                table: "BOOK_PHUONG_THUC_THANH_TOAN");

            migrationBuilder.DropIndex(
                name: "IX_BOOK_NHA_PHAN_PHOI_IsDeleted",
                table: "BOOK_NHA_PHAN_PHOI");

            migrationBuilder.AlterColumn<string>(
                name: "Ma",
                table: "BOOK_SACH",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_VOUCHER_Ten_IsDeleted",
                table: "BOOK_VOUCHER",
                columns: new[] { "Ten", "IsDeleted" },
                unique: true,
                filter: "IsDeleted <> 1");

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_THE_LOAI_SACH_Ten_IsDeleted",
                table: "BOOK_THE_LOAI_SACH",
                columns: new[] { "Ten", "IsDeleted" },
                unique: true,
                filter: "IsDeleted <> 1");

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_SACH_Ten_IsDeleted",
                table: "BOOK_SACH",
                columns: new[] { "Ten", "IsDeleted" },
                unique: true,
                filter: "IsDeleted <> 1");

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_PHUONG_THUC_THANH_TOAN_Ten_IsDeleted",
                table: "BOOK_PHUONG_THUC_THANH_TOAN",
                columns: new[] { "Ten", "IsDeleted" },
                unique: true,
                filter: "IsDeleted <> 1");

            migrationBuilder.CreateIndex(
                name: "IX_BOOK_NHA_PHAN_PHOI_Ten_IsDeleted",
                table: "BOOK_NHA_PHAN_PHOI",
                columns: new[] { "Ten", "IsDeleted" },
                unique: true,
                filter: "IsDeleted <> 1");
        }
    }
}
