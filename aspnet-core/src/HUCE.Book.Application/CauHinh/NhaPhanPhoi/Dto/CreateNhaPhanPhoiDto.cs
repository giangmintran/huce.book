﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using HUCE.Book.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book.CauHinh
{
    [AutoMapTo(typeof(NhaPhanPhoi))]
    public class CreateNhaPhanPhoiDto : EntityDto<int>
    {
        private string _ma;
        [Required(ErrorMessage = "Mã Nhà phân phối không được bỏ trống")]
        public string Ma 
        {
            get => _ma;
            set => _ma = value?.Trim();
        }

        private string _ten;
        [Required(ErrorMessage = "Tên Nhà phân phối không được bỏ trống")]
        public string Ten
        {
            get => _ten;
            set => _ten = value?.Trim();
        }

        [Phone(ErrorMessage = "Số điện thoại không đúng định dạng")]
        [StringLength(13, ErrorMessage = "Số điện thoại phải gồm 13 chữ số")]
        public string Sdt { get;set; }

        [Required(ErrorMessage = "Email không được bỏ trống")]
        public string Email { get; set; }

        private string _diaChi;
        [Required(ErrorMessage = "Địa chỉ không được bỏ trống")]
        public string DiaChi
        {
            get => _diaChi;
            set => _diaChi = value?.Trim();
        }

        private string _moTa;
        [Required(ErrorMessage = "Mô tả không được bỏ trống")]
        public string MoTa { 
            get => _moTa;
            set => _moTa = value?.Trim();
        }
    }
}
