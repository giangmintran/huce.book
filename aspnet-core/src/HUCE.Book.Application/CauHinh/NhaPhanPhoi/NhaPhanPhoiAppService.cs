﻿using Abp.Application.Services.Dto;
using Abp.Auditing;
using Abp.Authorization;
using Abp.Domain.Uow;
using Abp.UI;
using HUCE.Book.Authorization;
using HUCE.Book.CauHinh.Dto;
using HUCE.Book.Entities;
using HUCE.Book.EntityFrameworkCore.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book.CauHinh
{
    [AbpAuthorize]
    [DisableAuditing]
    public class NhaPhanPhoiAppService : BookAppServiceBase
    {
        private readonly NhaPhanPhoiRepository _nhaPhanPhoiRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public NhaPhanPhoiAppService 
            (NhaPhanPhoiRepository nhaPhanPhoiRepository,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _nhaPhanPhoiRepo = nhaPhanPhoiRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [AbpAuthorize(PermissionNames.Actions_NhaPhanPhoi_Create)]
        public async Task CreateAsync (CreateNhaPhanPhoiDto input)
        {
            var unitOfWork = _unitOfWorkManager.Begin(); //Tạo UnitOfWork

            if (_nhaPhanPhoiRepo.GetDbQueryTable().Any(d => d.Ma == input.Ma && !d.IsDeleted))
            {
                throw new UserFriendlyException($"Mã Nhà phân phối: {input.Ma} đã tồn tại");
            }
            
            _nhaPhanPhoiRepo.Insert(ObjectMapper.Map<NhaPhanPhoi>(input));
            CurrentUnitOfWork.SaveChanges();
            await unitOfWork.CompleteAsync();//Hoàn thành 
        }


        [AbpAuthorize(PermissionNames.Actions_NhaPhanPhoi_Update)]
        public async Task UpdateAsync (UpdateNhaPhanPhoiDto input)
        {
            if (_nhaPhanPhoiRepo.GetDbQueryTable().Any(d => d.Ma == input.Ma && !d.IsDeleted))
            {
                throw new UserFriendlyException($"Mã Nhà phân phối: {input.Ma} đã tồn tại");
            }
            var unitOfWork = _unitOfWorkManager.Begin();
            _nhaPhanPhoiRepo.Update(ObjectMapper.Map<NhaPhanPhoi>(input));
            await unitOfWork.CompleteAsync();
        }


        [AbpAuthorize(PermissionNames.Actions_NhaPhanPhoi_Delete)]
        public async Task DeleteAsync (EntityDto<int> input)
        {
            var unitOfWork = _unitOfWorkManager.Begin();
            var entity = _nhaPhanPhoiRepo.GetDbQueryTable().Where(d => !d.IsDeleted).FirstOrDefault(e => e.Id == input.Id);
            if (entity == null)
            {
                throw new UserFriendlyException("Không tìm thấy");
            }
            entity.IsDeleted = true;
            CurrentUnitOfWork.SaveChanges();
            await unitOfWork.CompleteAsync();
        }

        [AbpAuthorize]
        public async Task<NhaPhanPhoiDto> GetAsync (EntityDto<int> input)
        {
            var entity = _nhaPhanPhoiRepo.FirstOrDefault(e => e.Id == input.Id && !e.IsDeleted);
            if (entity == null)
            {
                throw new UserFriendlyException("Không tìm thấy Nhà phân phối");
            };
            return ObjectMapper.Map<NhaPhanPhoiDto>(entity);
        }


        [AbpAuthorize(PermissionNames.Pages_NhaPhanPhoi)]
        public async Task<PagedResultDto<NhaPhanPhoiDto>> GetAllAsync (PagedCauHinhRequestDtoBase input)
        {
            var result = new PagedResultDto<NhaPhanPhoiDto>();
            var query = _nhaPhanPhoiRepo.GetDbQueryTable().AsQueryable().Where(p => !p.IsDeleted);
            
            if (!string.IsNullOrEmpty(input.Keyword))
            {
                query = query.Where(o => o.Ma.Contains(input.Keyword)
                                    || o.Ten.Contains(input.Keyword));
            }

            var items = await query.OrderByDescending(o => o.Id)
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount)
                .ToListAsync();
            var phanPhois = ObjectMapper.Map<List<NhaPhanPhoiDto>>(items);
            result.Items = phanPhois;
            result.TotalCount = await query.CountAsync();
            return result;
        }

        public async Task<List<NhaPhanPhoiDto>> GetAllList()
        {
            var dsNhaPhanPhoi = await _nhaPhanPhoiRepo.GetAllListAsync();
            var result = ObjectMapper.Map<List<NhaPhanPhoiDto>>(dsNhaPhanPhoi);
            return result;
        }
    }
}
