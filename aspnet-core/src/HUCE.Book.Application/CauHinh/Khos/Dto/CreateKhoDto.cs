﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using HUCE.Book.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book.CauHinh
{
    [AutoMapTo(typeof(Kho))]
    public class CreateKhoDto : EntityDto<int>
    {
        public string Ma { get; set; }
        public string Ten { get; set; }
        public int SoLuong { get; set; }
        public string DiaChi { get; set; }
    }
}
