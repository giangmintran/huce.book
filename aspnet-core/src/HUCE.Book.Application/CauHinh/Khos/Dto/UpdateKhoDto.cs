﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using HUCE.Book.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book.CauHinh
{
    [AutoMapTo(typeof(Kho))]
    public class UpdateKhoDto : CreateKhoDto, IEntityDto<int>
    {
        public int Id { get; set; }
    }
}
