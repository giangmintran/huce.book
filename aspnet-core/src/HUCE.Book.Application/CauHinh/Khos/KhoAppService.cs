﻿using Abp.Application.Services.Dto;
using Abp.Auditing;
using Abp.Authorization;
using Abp.Domain.Uow;
using Abp.UI;
using HUCE.Book.Authorization;
using HUCE.Book.CauHinh.Dto;
using HUCE.Book.Entities;
using HUCE.Book.EntityFrameworkCore.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book.CauHinh
{
    [AbpAuthorize]
    [DisableAuditing]
    public class KhoAppService : BookAppServiceBase
    {
        private readonly KhoRepository _khoRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public KhoAppService
            (KhoRepository khoRepository,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _khoRepo = khoRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [AbpAuthorize(PermissionNames.Actions_Kho_Create)]
        public async Task CreateAsync(CreateKhoDto input)
        {
            var unitOfWork = _unitOfWorkManager.Begin(); //Tạo UnitOfWork

            if (_khoRepo.GetDbQueryTable().Any(d => d.Ma == input.Ma && !d.IsDeleted))
            {
                throw new UserFriendlyException($"Mã Nhà phân phối: {input.Ma} đã tồn tại");
            }

            _khoRepo.Insert(ObjectMapper.Map<Kho>(input));
            CurrentUnitOfWork.SaveChanges();
            await unitOfWork.CompleteAsync();//Hoàn thành 
        }


        [AbpAuthorize(PermissionNames.Actions_Kho_Update)]
        public async Task UpdateAsync(UpdateKhoDto input)
        {
            if (_khoRepo.GetDbQueryTable().Any(d => d.Ma == input.Ma && !d.IsDeleted))
            {
                throw new UserFriendlyException($"Mã Nhà phân phối: {input.Ma} đã tồn tại");
            }
            var unitOfWork = _unitOfWorkManager.Begin();
            _khoRepo.Update(ObjectMapper.Map<Kho>(input));
            await unitOfWork.CompleteAsync();
        }


        [AbpAuthorize(PermissionNames.Actions_Kho_Delete)]
        public async Task DeleteAsync(EntityDto<int> input)
        {
            var unitOfWork = _unitOfWorkManager.Begin();
            var entity = _khoRepo.GetDbQueryTable().Where(d => !d.IsDeleted).FirstOrDefault(e => e.Id == input.Id);
            if (entity == null)
            {
                throw new UserFriendlyException("Không tìm thấy");
            }
            entity.IsDeleted = true;
            CurrentUnitOfWork.SaveChanges();
            await unitOfWork.CompleteAsync();
        }

        [AbpAuthorize]
        public async Task<KhoDto> GetAsync(EntityDto<int> input)
        {
            var entity = _khoRepo.FirstOrDefault(e => e.Id == input.Id && !e.IsDeleted);
            if (entity == null)
            {
                throw new UserFriendlyException("Không tìm thấy Nhà phân phối");
            };
            return ObjectMapper.Map<KhoDto>(entity);
        }


        [AbpAuthorize(PermissionNames.Pages_Kho)]
        public async Task<PagedResultDto<KhoDto>> GetAllAsync(PagedCauHinhRequestDtoBase input)
        {
            var result = new PagedResultDto<KhoDto>();
            var query = _khoRepo.GetDbQueryTable().AsQueryable().Where(p => !p.IsDeleted);

            if (!string.IsNullOrEmpty(input.Keyword))
            {
                query = query.Where(o => o.Ma.Contains(input.Keyword)
                                    || o.Ten.Contains(input.Keyword));
            }

            var items = await query.OrderByDescending(o => o.Id)
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount)
                .ToListAsync();
            var phanPhois = ObjectMapper.Map<List<KhoDto>>(items);
            result.Items = phanPhois;
            result.TotalCount = await query.CountAsync();
            return result;
        }

        public async Task<List<KhoDto>> GetAllList()
        {
            var dsKho = await _khoRepo.GetAllListAsync();
            var result = ObjectMapper.Map<List<KhoDto>>(dsKho);
            return result;
        }
    }
}
