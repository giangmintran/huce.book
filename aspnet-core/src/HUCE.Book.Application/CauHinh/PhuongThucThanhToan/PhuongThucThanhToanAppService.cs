﻿using Abp.Application.Services.Dto;
using Abp.Domain.Uow;
using Abp.UI;
using HUCE.Book.CauHinh.Dto;
using HUCE.Book.Entities;
using HUCE.Book.EntityFrameworkCore.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book.CauHinh
{
    public class PhuongThucThanhToanAppService : BookAppServiceBase
    {
        private readonly PhuongThucThanhToanRepository _phuongThucThanhToanRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public PhuongThucThanhToanAppService
            (PhuongThucThanhToanRepository phuongThucThanhToanRepository,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _phuongThucThanhToanRepo = phuongThucThanhToanRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task CreateAsync(CreatePhuongThucThanhToanDto input)
        {
            var unitOfWork = _unitOfWorkManager.Begin(); //Tạo UnitOfWork

            if (_phuongThucThanhToanRepo.GetDbQueryTable().Any(d => d.Ten == input.Ten && !d.IsDeleted))
            {
                throw new UserFriendlyException($"Tên phương thức thanh toán: {input.Ten} đã tồn tại");
            }
            _phuongThucThanhToanRepo.Insert(ObjectMapper.Map<PhuongThucThanhToan>(input)); //Luu data, su dung Map de anh xa
            CurrentUnitOfWork.SaveChanges(); //lưu để tăng Id
            await unitOfWork.CompleteAsync();//Hoàn thành 
        }

        public async Task UpdateAsync(CreatePhuongThucThanhToanDto input)
        {
            if (_phuongThucThanhToanRepo.GetDbQueryTable().Any(d => d.Ten == input.Ten && !d.IsDeleted))
            {
                throw new UserFriendlyException($"Tên Phương Thức Thanh Toán: {input.Ten} đã tồn tại");
            }
            var unitOfWork = _unitOfWorkManager.Begin();
            _phuongThucThanhToanRepo.Update(ObjectMapper.Map<PhuongThucThanhToan>(input));
            await unitOfWork.CompleteAsync();
        }

        public async Task DeleteAsync(EntityDto<int> input)
        {
            var unitOfWork = _unitOfWorkManager.Begin();
            var entity = _phuongThucThanhToanRepo.GetDbQueryTable().Where(d => !d.IsDeleted).FirstOrDefault(e => e.Id == input.Id);
            if (entity == null)
            {
                throw new UserFriendlyException("Không tìm thấy");
            }
            entity.IsDeleted = true;
            CurrentUnitOfWork.SaveChanges();
            await unitOfWork.CompleteAsync();
        }

        public async Task<PhuongThucThanhToanDto> GetAsync(EntityDto<int> input)
        {
            var entity = _phuongThucThanhToanRepo.FirstOrDefault(e => e.Id == input.Id && !e.IsDeleted);
            if (entity == null)
            {
                throw new UserFriendlyException("Không tìm thấy nhà phân phối");
            };
            return ObjectMapper.Map<PhuongThucThanhToanDto>(entity);
        }

        public async Task<PagedResultDto<PhuongThucThanhToanDto>> GetAllAsync(PagedCauHinhRequestDtoBase input)
        {
            var result = new PagedResultDto<PhuongThucThanhToanDto>();
            var query = _phuongThucThanhToanRepo.GetDbQueryTable().AsQueryable().Where(p => !p.IsDeleted);

            if (!string.IsNullOrEmpty(input.Keyword))
            {
                query = query.Where(o => o.Ten.Contains(input.Keyword)
                                    || o.Ten.Contains(input.Keyword));
            }

            var items = await query.OrderByDescending(o => o.Id)
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount)
                .ToListAsync();
            var phuongThucThanhToans = ObjectMapper.Map<List<PhuongThucThanhToanDto>>(items);

            result.Items = phuongThucThanhToans;
            result.TotalCount = await query.CountAsync();
            return result;
        }
    }
}
