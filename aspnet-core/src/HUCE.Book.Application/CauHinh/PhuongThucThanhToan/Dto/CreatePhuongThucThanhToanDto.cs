﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using HUCE.Book.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book.CauHinh
{
    [AutoMapTo(typeof(PhuongThucThanhToan))]
    public class CreatePhuongThucThanhToanDto : EntityDto<int>
    {
        private string _ten;
        [Required(ErrorMessage = "Tên phương thức thanh toán không được bỏ trống")]
        public string Ten 
        {
            get => _ten;
            set => _ten = value?.Trim();
        }

        private string _mota;
        [Required(ErrorMessage = "Mô tả không được bỏ trống")]
        public string MoTa
        {
            get => _mota;
            set => _mota = value?.Trim();
        }
    }
}
