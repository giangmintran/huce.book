﻿using Abp.Application.Services.Dto;
using Abp.Auditing;
using Abp.Authorization;
using Abp.Domain.Uow;
using Abp.UI;
using AutoMapper;
using HUCE.Book.Authorization;
using HUCE.Book.CauHinh.Dto;
using HUCE.Book.CauHinh.Voucher.Dto;
using HUCE.Book.Entities;
using HUCE.Book.EntityFrameworkCore.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book
{
    [AbpAuthorize]
    [DisableAuditing]
    public class VoucherAppService: BookAppServiceBase
    {
        private readonly VoucherRepository _voucherRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public VoucherAppService
            (VoucherRepository voucherRepository,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _voucherRepo = voucherRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [AbpAuthorize(PermissionNames.Actions_Voucher_Create)]
        public async Task CreateAsync(CreateVoucherDto input)
        {
            var unitOfWork = _unitOfWorkManager.Begin(); //Tạo UnitOfWork

            if (_voucherRepo.GetDbQueryTable().Any(d => d.MaVoucher == input.MaVoucher && !d.IsDeleted))
            {
                throw new UserFriendlyException($"Trùng mã voucher: {input.Ten} vui lòng nhập mã khác");
            }
            _voucherRepo.Insert(ObjectMapper.Map<Voucher>(input)); //Luu data, su dung Map de anh xa
            CurrentUnitOfWork.SaveChanges(); //lưu để tăng Id
            await unitOfWork.CompleteAsync();//Hoàn thành 
        }

        [AbpAuthorize(PermissionNames.Actions_Voucher_Update)]
        public async Task UpdateAsync(CreateVoucherDto input)
        {
            if (_voucherRepo.GetDbQueryTable().Any(d => d.MaVoucher == input.MaVoucher && !d.IsDeleted))
            {
                throw new UserFriendlyException($"Mã voucher: {input.MaVoucher} đã tồn tại");
            }
            var unitOfWork = _unitOfWorkManager.Begin();
            _voucherRepo.Update(ObjectMapper.Map<Voucher>(input));
            await unitOfWork.CompleteAsync();
        }

        [AbpAuthorize(PermissionNames.Actions_Voucher_Delete)]
        public async Task DeleteAsync(EntityDto<int> input)
        {
            var unitOfWork = _unitOfWorkManager.Begin();
            var entity = _voucherRepo.GetDbQueryTable().Where(d => !d.IsDeleted).FirstOrDefault(e => e.Id == input.Id);
            if (entity == null)
            {
                throw new UserFriendlyException("Không tìm thấy Voucher này");
            }
            entity.IsDeleted = true;
            CurrentUnitOfWork.SaveChanges();
            await unitOfWork.CompleteAsync();
        }

        [AbpAuthorize]
        public async Task<VoucherDto> GetAsync(EntityDto<int> input)
        {
            var entity = _voucherRepo.FirstOrDefault(e => e.Id == input.Id && !e.IsDeleted);
            if (entity == null)
            {
                throw new UserFriendlyException("Không tìm thấy Voucher này");
            };
            return ObjectMapper.Map<VoucherDto>(entity);
        }

        [AbpAuthorize(PermissionNames.Pages_Voucher)]
        public async Task<PagedResultDto<VoucherDto>> GetAllAsync(PagedCauHinhRequestDtoBase input)
        {
            var result = new PagedResultDto<VoucherDto>();
            var query = _voucherRepo.GetDbQueryTable().AsQueryable().Where(p => !p.IsDeleted);

            if (!string.IsNullOrEmpty(input.Keyword))
            {
                query = query.Where(o => o.MaVoucher.Contains(input.Keyword));
            }

            var items = await query.OrderByDescending(o => o.Id)
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount)
                .ToListAsync();
            var vouchers = ObjectMapper.Map<List<VoucherDto>>(items);

            result.Items = vouchers;
            result.TotalCount = await query.CountAsync();
            return result;
        }

        public async Task<List<VoucherDto>> GetAllList()
        {
            var ds = await _voucherRepo.GetAllListAsync();
            var result = ObjectMapper.Map<List<VoucherDto>>(ds);
            return result;
        }
    }
}
