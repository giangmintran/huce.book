﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book.CauHinh.Voucher.Dto
{
    [AutoMapTo(typeof(VoucherDto))]

    public class UpdateVoucherDto
    {
        public int Id { get; set; }
    }
}
