﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;
using HUCE.Book.Entities;

namespace HUCE.Book
{
    [AutoMapTo(typeof(Voucher))]

    public class CreateVoucherDto
    {
        private string _maVoucher;
        [Required(ErrorMessage = "Mã Voucher không được bỏ trống")]
        public string MaVoucher 
        {
            get => _maVoucher;
            set => _maVoucher = value?.Trim();
        }
        private string _ten;
        [Required(ErrorMessage = "tên Voucher không được bỏ trống")]
        public string Ten 
        {
            get => _ten;
            set => _ten = value?.Trim();
        }

        public int? LoaiVoucher { get; set; }


        public decimal? Gia { get; set; }


        private string _mota;
        public string MoTa
        {
            get => _mota;
            set => _mota = value?.Trim();
        }


        [Required(ErrorMessage = "Ngày bắt đầu giảm giá không được bỏ trống")]
        public DateTime NgayBatDau { get; set; }


        [Required(ErrorMessage = "Ngày hết hạn giảm giá không được bỏ trống")]
        public DateTime NgayKetThuc { get; set; }
    }
}
