﻿using Abp.AutoMapper;
using HUCE.Book.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book
{
    [AutoMapFrom(typeof(Voucher))]

    public class VoucherDto
    {
        public string MaVoucher { get; set; }
        public string Ten { get; set; }
        public int? LoaiVoucher { get; set; }
        public decimal? Gia { get; set; }
        public string MoTa { get; set; }
        public DateTime NgayBatDau { get; set; }
        public DateTime NgayKetThuc { get; set; }
    }
}
