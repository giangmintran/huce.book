﻿using Abp.Application.Services.Dto;
using Abp.Domain.Uow;
using Abp.UI;
using HUCE.Book.CauHinh;
using HUCE.Book.CauHinh.Dto;
using HUCE.Book.Entities;
using HUCE.Book.EntityFrameworkCore.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book
{
    public class TheLoaiSachAppService: BookAppServiceBase
    {
        private readonly TheLoaiSachRepository _theLoaiSachRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public TheLoaiSachAppService
            (TheLoaiSachRepository theLoaiSachRepository,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _theLoaiSachRepo = theLoaiSachRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task CreateAsync(CreateTheLoaiSachDto input)
        {
            var unitOfWork = _unitOfWorkManager.Begin(); //Tạo UnitOfWork

            if (_theLoaiSachRepo.GetDbQueryTable().Any(d => d.Ten == input.Ten && !d.IsDeleted))
            {
                throw new UserFriendlyException($"Tên Sách: {input.Ten} đã tồn tại");
            }
            _theLoaiSachRepo.Insert(ObjectMapper.Map<TheLoaiSach>(input)); //Luu data, su dung Map de anh xa
            CurrentUnitOfWork.SaveChanges(); //lưu để tăng Id
            await unitOfWork.CompleteAsync();//Hoàn thành 
        }

        public async Task UpdateAsync(CreateTheLoaiSachDto input)
        {
            if (_theLoaiSachRepo.GetDbQueryTable().Any(d => d.Ten == input.Ten && !d.IsDeleted))
            {
                throw new UserFriendlyException($"Mã Nhà phân phối: {input.Ten} đã tồn tại");
            }
            var unitOfWork = _unitOfWorkManager.Begin();
            _theLoaiSachRepo.Update(ObjectMapper.Map<TheLoaiSach>(input));
            await unitOfWork.CompleteAsync();
        }

        public async Task DeleteAsync(EntityDto<int> input)
        {
            var unitOfWork = _unitOfWorkManager.Begin();
            var entity = _theLoaiSachRepo.GetDbQueryTable().Where(d => !d.IsDeleted).FirstOrDefault(e => e.Id == input.Id);
            if (entity == null)
            {
                throw new UserFriendlyException("Không tìm thấy thể loại này");
            }
            entity.IsDeleted = true;
            CurrentUnitOfWork.SaveChanges();
            await unitOfWork.CompleteAsync();
        }

        public async Task<TheLoaiSachDto> GetAsync(EntityDto<int> input)
        {
            var entity = _theLoaiSachRepo.FirstOrDefault(e => e.Id == input.Id && !e.IsDeleted);
            if (entity == null)
            {
                throw new UserFriendlyException("Không tìm thấy Thể loại sách");
            };
            return ObjectMapper.Map<TheLoaiSachDto>(entity);
        }

        public async Task<PagedResultDto<TheLoaiSachDto>> GetAllAsync(PagedCauHinhRequestDtoBase input)
        {
            var result = new PagedResultDto<TheLoaiSachDto>();
            var query = _theLoaiSachRepo.GetDbQueryTable().AsQueryable().Where(p => !p.IsDeleted);

            if (!string.IsNullOrEmpty(input.Keyword))
            {
                query = query.Where(o => o.Ten.Contains(input.Keyword));
            }

            var items = await query.OrderByDescending(o => o.Id)
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount)
                .ToListAsync();
            var theLoaiSachs = ObjectMapper.Map<List<TheLoaiSachDto>>(items);

            result.Items = theLoaiSachs;
            result.TotalCount = await query.CountAsync();
            return result;
        }
        public async Task<List<TheLoaiSachDto>> GetAllList()
        {
            var ds = await _theLoaiSachRepo.GetAllListAsync();
            var result = ObjectMapper.Map<List<TheLoaiSachDto>>(ds);
            return result;
        }
    }
}
