﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using HUCE.Book.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book.CauHinh
{
    [AutoMapFrom(typeof(TheLoaiSach))]

    public class TheLoaiSachDto : EntityDto<int>
    {
        public string Ten { get; set; }
        public string MoTa { get; set; }
    }
}
