﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using HUCE.Book.CauHinh;
using HUCE.Book.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book
{
    [AutoMapTo(typeof(TheLoaiSach))]

    public class CreateTheLoaiSach : CreateTheLoaiSachDto, IEntityDto<int>
    {
        public int Id { get; set; }
    }
}
