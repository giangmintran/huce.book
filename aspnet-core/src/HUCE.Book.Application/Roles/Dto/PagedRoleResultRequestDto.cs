﻿using Abp.Application.Services.Dto;

namespace HUCE.Book.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

