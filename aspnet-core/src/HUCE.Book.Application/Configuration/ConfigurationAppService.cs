﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using HUCE.Book.Configuration.Dto;

namespace HUCE.Book.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : BookAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
