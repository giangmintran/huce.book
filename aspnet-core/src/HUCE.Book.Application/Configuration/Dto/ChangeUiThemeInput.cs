﻿using System.ComponentModel.DataAnnotations;

namespace HUCE.Book.Configuration.Dto
{
    public class ChangeUiThemeInput
    {
        [Required]
        [StringLength(32)]
        public string Theme { get; set; }
    }
}
