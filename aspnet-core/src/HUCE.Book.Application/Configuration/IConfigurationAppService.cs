﻿using System.Threading.Tasks;
using HUCE.Book.Configuration.Dto;

namespace HUCE.Book.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
