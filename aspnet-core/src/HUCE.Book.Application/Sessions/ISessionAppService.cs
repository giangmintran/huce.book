﻿using System.Threading.Tasks;
using Abp.Application.Services;
using HUCE.Book.Sessions.Dto;

namespace HUCE.Book.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
