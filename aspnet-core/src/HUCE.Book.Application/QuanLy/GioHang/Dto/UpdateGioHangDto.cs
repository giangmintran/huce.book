﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using HUCE.Book.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book.QuanLy
{
    [AutoMapTo(typeof(GioHang))]
    public class UpdateGioHangDto : CreateGioHangDto, IEntityDto<int>
    {
        public int Id { get; set; }
    }

    [AutoMapTo(typeof(ChiTietGioHang))]
    public class UpdateChiTietGioHangDto: CreateChiTietGioHangDto, IEntityDto<int>
    {
        public int Id { get; set; }
    }
}
