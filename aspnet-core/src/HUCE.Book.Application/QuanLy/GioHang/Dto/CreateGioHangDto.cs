﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using HUCE.Book.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book.QuanLy
{
    [AutoMapTo(typeof(GioHang))]
    public class CreateGioHangDto : EntityDto<int>
    {
        public List<CreateChiTietGioHangDto> ChiTietGioHangs { get; set; }
    }

    [AutoMapTo(typeof(ChiTietGioHang))]
    public class CreateChiTietGioHangDto : EntityDto<int>
    {
        public int? GioHangId { get; set; }
        public int? SachId { get; set; }
        public double Gia { get; set; }
        public int? SoLuong { get; set; }
    }
}
