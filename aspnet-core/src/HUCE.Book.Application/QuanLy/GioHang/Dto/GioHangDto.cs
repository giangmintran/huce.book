﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using HUCE.Book.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book.QuanLy
{
    [AutoMapFrom(typeof(GioHang))]
    public class GioHangDto : EntityDto<int>
    {
        public long? CreatorUserId { get; set; }
        public List<ChiTietGioHangDto> ChiTietGioHangs { get; set; }
    }

    [AutoMapFrom(typeof(ChiTietGioHang))]
    public class ChiTietGioHangDto : EntityDto<int>
    {
        public int? GioHangId { get; set; }
        public int? SachId { get; set; }
        public double Gia { get; set; }
        public int? SoLuong { get; set; }
    }
}
