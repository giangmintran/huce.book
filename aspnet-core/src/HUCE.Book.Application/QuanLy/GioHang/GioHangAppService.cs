﻿using Abp.Application.Services.Dto;
using Abp.Domain.Uow;
using Abp.UI;
using HUCE.Book.Common.Dto;
using HUCE.Book.Entities;
using HUCE.Book.EntityFrameworkCore.Repositories;
using HUCE.Book.QuanLy;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book.QuanLy
{
    public class GioHangAppService : BookAppServiceBase
    {
        private readonly GioHangRepository _gioHangRepo;
        private readonly ChiTietGioHangRepository _chiTietGioHangRepo;
        private readonly IUnitOfWorkManager _unitOfWork;
        private readonly SachRepository _sachRepo;

        public GioHangAppService(GioHangRepository gioHangRepo, ChiTietGioHangRepository chiTietGioHang, 
            SachRepository sachRepo, IUnitOfWorkManager unitOfWork)
        {
            _gioHangRepo = gioHangRepo;
            _chiTietGioHangRepo = chiTietGioHang;
            _sachRepo = sachRepo;
            _unitOfWork = unitOfWork;
        }

        public async Task CreateAsync(CreateGioHangDto input)
        {
            if (await _gioHangRepo.GetDbQueryTable().AnyAsync(d => d.Id == input.Id && !d.IsDeleted))
            {
                throw new UserFriendlyException("Giỏ hàng đã tồn tại");
            }
            var unitOfWork = _unitOfWork.Begin();
            var gioHang = ObjectMapper.Map<GioHang>(input);
            _gioHangRepo.Insert(gioHang);

            CurrentUnitOfWork.SaveChanges(); //lưu để lấy id
            foreach (var chiTietGioHang in input.ChiTietGioHangs)
            {
                var chiTietGioHangInsert = ObjectMapper.Map<ChiTietGioHang>(chiTietGioHang);
                chiTietGioHangInsert.GioHangId = gioHang.Id;
                _chiTietGioHangRepo.Insert(chiTietGioHangInsert);
            }
            await unitOfWork.CompleteAsync();
        }

        public async Task CreateChiTietAsync(CreateChiTietGioHangDto input)
        {
            var unitOfWork = _unitOfWork.Begin();
            var chiTietGioHang = ObjectMapper.Map<ChiTietGioHang>(input);
            _chiTietGioHangRepo.Insert(chiTietGioHang);
            CurrentUnitOfWork.SaveChanges(); //lưu để lấy id
            await unitOfWork.CompleteAsync();
        }

        public async Task UpdateAsync(UpdateGioHangDto input)
        {
            var gioHangQuery = _gioHangRepo.GetDbQueryTable().Where(d => d.Id == input.Id && !d.IsDeleted);
            if (gioHangQuery == null)
            {
                throw new UserFriendlyException($"Không tìm thấy đơn hàng có id: {input.Id}");
            }

            using var unitOfWork = _unitOfWork.Begin();
            var gioHangUpdate = ObjectMapper.Map<GioHang>(input);
            _gioHangRepo.Update(gioHangUpdate);

            var chiTietQuery = gioHangQuery.Join(_chiTietGioHangRepo.GetDbQueryTable(), d => d.Id,
                c => c.GioHangId, (GioHang, ChiTietGioHang) => ChiTietGioHang);
            
            foreach (var chiTietGioHang in input.ChiTietGioHangs)
            {
                var chiTietUpdate = ObjectMapper.Map<ChiTietGioHang>(chiTietGioHang);
                chiTietUpdate.GioHangId = gioHangUpdate.Id; 
                if (chiTietUpdate.Id != 0) //có rồi thì cập nhật  
                {     
                    _chiTietGioHangRepo.Update(chiTietUpdate);
                }
                else //
                {
                    _chiTietGioHangRepo.Insert(chiTietUpdate);
                }
            }
            await unitOfWork.CompleteAsync();
        }

        public async Task DeleteAsync(EntityDto<int> input)
        {
            var unitOfWork = _unitOfWork.Begin();
            var entity = _gioHangRepo.FirstOrDefault(e => e.Id == input.Id);
            if (entity == null)
            {
                throw new UserFriendlyException($"Không tìm thấy đơn hàng có id: {input.Id}");
            }
            entity.IsDeleted = true;
            var childs = _chiTietGioHangRepo.GetDbQueryTable().Where(c => c.GioHangId == input.Id && !c.IsDeleted).ToList();
            foreach (var chiTiet in childs)
            {
                chiTiet.IsDeleted = true;
            }
            CurrentUnitOfWork.SaveChanges();
            await unitOfWork.CompleteAsync();
        }

        public async Task<GioHangDto> GetAsync(EntityDto<int> input)
        {
            var gioHangQuery = await _gioHangRepo.FirstOrDefaultAsync(d => d.Id == input.Id && !d.IsDeleted);
            if (gioHangQuery == null)
            {
                throw new UserFriendlyException("Không tìm thấy giỏ hàng");
            }

            var chiTietGioHangQuery = _chiTietGioHangRepo.GetDbQueryTable().Where(c => c.GioHangId == gioHangQuery.Id && !c.IsDeleted);
            var chiTiet = await chiTietGioHangQuery.Select(c => new ChiTietGioHangDto
            {
                GioHangId = c.GioHangId,
                SachId = c.SachId,
                Gia = c.Gia,
                SoLuong = c.SoLuong
            }).ToListAsync();

            var result = ObjectMapper.Map<GioHangDto>(gioHangQuery);
            result.ChiTietGioHangs = chiTiet;
            return result;
        }

        public async Task<PagedResultDto<GioHangDto>> GetAllAsync(PagedRequestDtoBase input)
        {
            var result = new PagedResultDto<GioHangDto>();
            var query = _gioHangRepo.GetDbQueryTable().AsQueryable().Where(p => !p.IsDeleted);
            //var gioHangQuery = _gioHangRepo.GetAllListAsync();
            var chiTietQuery = _chiTietGioHangRepo.GetDbQueryTable().Where(c => !c.IsDeleted);

            var items = await query.OrderByDescending(o => o.Id)
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount)
                .ToListAsync();

            /*if (!string.IsNullOrEmpty(input.Keyword))
            {
                throw new UserFriendlyException($"Không tìm thấy đơn chi tiết có id");
            }*/

            var gioHangs = ObjectMapper.Map<List<GioHangDto>>(items);

            foreach (var gioHang in gioHangs)
            {
                var chiTiets = chiTietQuery.Where(c => c.GioHangId == gioHang.Id);
                var chiTiet = await chiTiets.Select(c => new ChiTietGioHangDto {
                    GioHangId = c.GioHangId,
                    SachId = c.SachId,
                    Gia = c.Gia,
                    SoLuong = c.SoLuong
                }).ToListAsync();
                gioHang.ChiTietGioHangs = chiTiet;
            }
            result.Items = gioHangs;
            result.TotalCount = await query.CountAsync();
            return result;
        }

        public async Task<ChiTietGioHangDto> GetChiTietAsync(EntityDto<int> input)
        {
            var chiTietQuery = _chiTietGioHangRepo.FirstOrDefaultAsync(c => c.Id == input.Id && !c.IsDeleted);
            if (chiTietQuery == null)
            {
                throw new UserFriendlyException($"Không tìm thấy đơn chi tiết có id {input.Id}");
            }
            var result = ObjectMapper.Map<ChiTietGioHangDto>(chiTietQuery);
            return result;
        }

        /// <summary>
        /// Lấy tất cả giỏ hàng không phân trang
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        public async Task<List<GioHangDto>> GetAllListAsync()
        {
            var gioHangs = await _gioHangRepo.GetDbQueryTable()
                .Where(d => !d.IsDeleted)
                .OrderBy(d => d.Id)
                .ThenByDescending(d => d.Id)
                .ToListAsync();
            return ObjectMapper.Map<List<GioHangDto>>(gioHangs);
        }
    }
}
