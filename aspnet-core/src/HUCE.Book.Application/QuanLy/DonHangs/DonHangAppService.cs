﻿using Abp.Application.Services.Dto;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using Abp.UI;
using HUCE.Book.Authorization.Users;
using HUCE.Book.Entities;
using HUCE.Book.EntityFrameworkCore.Repositories;
using HUCE.Book.QuanLy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book.QuanLy
{
    public class DonHangAppService : BookAppServiceBase
    {
        private readonly DonHangRepository _donHangRepo;
        private readonly IUnitOfWorkManager _unitOfWork;
        private readonly SachRepository _sachRepo;
        private readonly GioHangRepository _gioHangRepo;
        private readonly ChiTietGioHangRepository _chiTietGioHangRepo;

        public IAbpSession AbpSession { get; set; }

        public DonHangAppService(DonHangRepository donHangRepo, SachRepository sachRepo, IUnitOfWorkManager unitOfWork,GioHangRepository gioHang, ChiTietGioHangRepository chiTietGioHang)
        {
            _donHangRepo = donHangRepo;
            _unitOfWork = unitOfWork;
            _sachRepo = sachRepo;
            _gioHangRepo = gioHang;
            _chiTietGioHangRepo = chiTietGioHang;
            AbpSession = NullAbpSession.Instance;
        }

        /// <summary>
        /// Default đơn hàng chưa được duyệt
        /// Dùng để phê duyệt đơn hàng sau khi xử lý
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task PheDuyetDonHangAsync(int id)
        {

            var unitOfWork = _unitOfWork.Begin();
            var donHang = _donHangRepo.FirstOrDefault(d => d.Id == id);
            if (donHang == null)
            {
                throw new UserFriendlyException("Không tìm thấy đơn hàng");
            }

            donHang.IsDuyet = true;
            CurrentUnitOfWork.SaveChanges();
            await unitOfWork.CompleteAsync();
        }

        /// <summary>
        /// Default đơn hàng chưa được duyệt
        /// Dùng để phê duyệt đơn hàng sau khi xử lý
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        /// 
        /*public async Task PheDuyet(EntityDto<int> input)
        {
            await _donHangRepo.UpdateAsync(input.Id, async (entity) =>
            {
                entity.IsDuyet = true;
            });
        }*/

        /// <summary>
        /// Tạo đơn hàng và các mặt hàng sách tương ứng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task CreateAsync(CreateDonHangDto input)
        {
            if (_donHangRepo.GetDbQueryTable().Any(d => d.Ma == input.Ma && !d.IsDeleted))
            {
                throw new UserFriendlyException($"Mã {input.Ma} đã tồn tại");
            }
            using var unitOfWork = _unitOfWork.Begin();
            var donHang = ObjectMapper.Map<DonHang>(input);
            _donHangRepo.Insert(donHang);

            CurrentUnitOfWork.SaveChanges(); //lưu để lấy id

            await unitOfWork.CompleteAsync();
        }

        /// <summary>
        /// Tạo đơn hàng và các mặt hàng sách tương ứng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        /*public async Task CreateChiTietAsync(CreateChiTietDonHangDto input)
        {
            using var unitOfWork = _unitOfWork.Begin();
            var donHang = ObjectMapper.Map<ChiTietDonHangs>(input);
            _chiTietDonHangRepo.Insert(donHang);

            CurrentUnitOfWork.SaveChanges(); //lưu để lấy id
            await unitOfWork.CompleteAsync();
        }*/

        /// <summary>
        /// Cập nhật đơn hàng và đơn chi tiết
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task UpdateAsync(UpdateDonHangDto input)
        {
            var donHangQuery = _donHangRepo.GetDbQueryTable().Where(d => d.Id == input.Id && !d.IsDeleted && !d.IsDuyet);
            //Check trường hợp các phần tử trùng mã
            if (donHangQuery.Any(d => d.Id != input.Id && d.Ma == input.Ma))
            {
                throw new UserFriendlyException($"Mã {input.Ma} đã trùng với mã đơn hàng khác");
            }

            using var unitOfWork = _unitOfWork.Begin();
            var donHangUpdate = ObjectMapper.Map<DonHang>(input);
            _donHangRepo.Update(donHangUpdate);

            
            /*var chiTietQuery = donHangQuery.Join(_chiTietDonHangRepo.GetDbQueryTable(), d => d.Id, c => c.DonHangId,
                (DonHang, chiTietDonHang) => chiTietDonHang);

            foreach (var chiTietDonHang in input.ChiTietDonHangs)
            {
                var chiTietUpdate = ObjectMapper.Map<ChiTietDonHangs>(chiTietDonHang);
                chiTietUpdate.DonHangId = donHangUpdate.Id;
                if (chiTietUpdate.Id != 0) //có rồi thì cập nhật
                {
                    if (chiTietQuery.Any(c => c.Id != chiTietDonHang.Id))
                    {
                        throw new UserFriendlyException($" {chiTietDonHang.Id} đã tồn tại");
                    }
                    _chiTietDonHangRepo.Update(chiTietUpdate);
                }
                else //chưa có thì thêm
                {
                    _chiTietDonHangRepo.Insert(chiTietUpdate);
                }
            }*/
            await unitOfWork.CompleteAsync();
        }

       /* public async Task UpdateTrangThaiSanPham (UpdateChiTietDonHangDto input )
        {
            var query = _chiTietDonHangRepo.GetDbQueryTable().Where(c => c.Id == input.Id && !c.IsDeleted);
            var unitOfWork = _unitOfWork.Begin();
            var chiTietUpdate = new ChiTietDonHangs { TrangThai = input.TrangThai };

            _chiTietDonHangRepo.Update(chiTietUpdate);

            await unitOfWork.CompleteAsync();//Hoàn thành 
        }*/

        /// <summary>
        /// Xóa đơn hàng
        /// Dùng để phê duyệt đơn hàng sau khi xử lý
        /// </summary>
        /// <param name="input"></param>
        /// <returns>Xóa đơn hàng</returns>
        public async Task DeleteAsync(EntityDto<int> input)
        {
            var unitOfWork = _unitOfWork.Begin();
            var entity = _donHangRepo.FirstOrDefault(e => e.Id == input.Id);
            if (entity == null)
            {
                throw new UserFriendlyException($"Không tìm thấy đơn hàng có id: {input.Id}");
            }
            entity.IsDeleted = true;
            CurrentUnitOfWork.SaveChanges();
            await unitOfWork.CompleteAsync();
        }

        /// <summary>
        /// Lấy thông tin đơn hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<DonHangDto> GetAsync(EntityDto<int> input)
        {
            var donHang = await _donHangRepo.FirstOrDefaultAsync(d => d.Id == input.Id && !d.IsDeleted);
            if (donHang == null)
            {
                throw new UserFriendlyException("Không tìm thấy đơn hàng");
            }

            var gioHangQuery = await _gioHangRepo.FirstOrDefaultAsync(c => c.Id == donHang.GioHangId && !c.IsDeleted);

            var result = ObjectMapper.Map<DonHangDto>(donHang);
            var chiTietGioHangQuery = _chiTietGioHangRepo.GetDbQueryTable().AsNoTracking().Where(c => c.GioHangId == gioHangQuery.Id && !c.IsDeleted);

            var chiTiet = await chiTietGioHangQuery.Select(c => new ChiTietGioHangDto
            {
                GioHangId = c.GioHangId,
                SachId = c.SachId,
                Gia = c.Gia,
                SoLuong = c.SoLuong
            }).ToListAsync();
            result.ChiTietGioHangs = chiTiet;

            return result;
        }

        /// <summary>
        /// Lấy tất cả các đơn hàng có phân trang + set page
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<DonHangDto>> GetAllAsync(PagedDonHangRequestDto input)
        {
            var result = new PagedResultDto<DonHangDto>();
            var query = _donHangRepo.GetDbQueryTable().AsQueryable().Where(p => !p.IsDeleted);
            var donHangQuery = await _donHangRepo.GetAllListAsync();
            if (!string.IsNullOrEmpty(input.Keyword))
            {
                query = query.Where(o => o.Ma.Contains(input.Keyword));
            }

            if (input.IsDuyet.HasValue)
            {
                query = query.Where(x => x.IsDuyet == input.IsDuyet);
            }

            var items = await query.OrderByDescending(o => o.Id)
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount)
                .ToListAsync();

            var donHangs = ObjectMapper.Map<List<DonHangDto>>(items);
            /*var gioHangs = await _gioHangRepo.FirstOrDefaultAsync(p => p.Id == donHangs.GioHangId && !p.IsDeleted);
            var chiTietQuery = _chiTietGioHangRepo.GetDbQueryTable().Where(c => !c.IsDeleted);

            foreach (var gioHang in donHangs)
            {
                var chiTiets = chiTietQuery.Where(c => c.GioHangId == gioHang.Id);
                var chiTiet = await chiTiets.Select(c => new ChiTietGioHangDto
                {
                    GioHangId = c.GioHangId,
                    SachId = c.SachId,
                    Gia = c.Gia,
                    SoLuong = c.SoLuong
                }).ToListAsync();
                gioHang.ChiTietGioHangs = chiTiet;
            }*/
            result.Items = donHangs;
            result.TotalCount = await query.CountAsync();
            return result;
        }

        /// <summary>
        /// Lấy thông tin 1 chi tiết đơn hàng có trong đơn hàng theo id
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
       /* public async Task<ChiTietDonHangDto> GetChiTietAsync(EntityDto<int> input)
        {
            var chiTietQuery = await _chiTietDonHangRepo.FirstOrDefaultAsync(d => d.Id == input.Id && !d.IsDeleted);
            if (chiTietQuery == null)
            {
                throw new UserFriendlyException($"Không tìm thấy đơn chi tiết có id {input.Id}");
            }
            var result = ObjectMapper.Map<ChiTietDonHangDto>(chiTietQuery);
            return result;
        }*/


        /// <summary>
        /// Lấy tất cả các đơn hàng đã được tạo theo người dùng hiện hành đang đăng nhập
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
       /* public async Task<PagedResultDto<DonHangDto>> GetAllByUserAsync(PagedDonHangRequestDto input)
        {
            var currentUserId = AbpSession.UserId;
            var result = new PagedResultDto<DonHangDto>();
            var query = _donHangRepo.GetDbQueryTable().AsQueryable().Where(p => !p.IsDeleted && p.CreatorUserId == currentUserId);
            var donHangQuery = await _donHangRepo.GetAllListAsync();
            var chiTietQuery = _chiTietDonHangRepo.GetDbQueryTable().Where(c => !c.IsDeleted);
            if (!string.IsNullOrEmpty(input.Keyword))
            {
                query = query.Where(o => o.Ma.Contains(input.Keyword));
            }

            if (input.IsDuyet.HasValue)
            {
                query = query.Where(x => x.IsDuyet == input.IsDuyet);
            }

            var items = await query.OrderByDescending(o => o.Id)
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount)
                .ToListAsync();

            var donHangs = ObjectMapper.Map<List<DonHangDto>>(items);
            foreach (var donHang in donHangs)
            {
                var chiTiets = chiTietQuery.Where(c => c.DonHangId == donHang.Id);
                var chiTiet = await chiTiets.Select(c => new ChiTietDonHangDto
                {
                    Id = c.Id,
                    DonHangId = c.DonHangId,
                    SachId = c.SachId,
                    SoLuong = c.SoLuong
                }).ToListAsync();
                donHang.ChiTietDonHangs = chiTiet;
            }
            result.Items = donHangs;
            result.TotalCount = await query.CountAsync();
            return result;
        }*/

        /// <summary>
        /// Lấy tất cả đơn hàng không phân trang
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        public async Task<List<DonHangDto>> GetAllListAsync()
        {
            var donHangs = await _donHangRepo.GetDbQueryTable()
                .Where(d => !d.IsDeleted)
                .OrderBy(d => d.Id)
                .ThenByDescending(d => d.Id)
                .ToListAsync();
            return ObjectMapper.Map<List<DonHangDto>>(donHangs);
        }
    }
}
