﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using HUCE.Book.DataAnnotations;
using HUCE.Book.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book.QuanLy
{
    [AutoMapTo(typeof(DonHang))]
    public class CreateDonHangDto : EntityDto<int>
    {
        public string Ma { get; set; }
        public int? GioHangId { get; set; }
        public int? VoucherId { get; set; }

        //[Required(ErrorMessage = "Số điện thoại không được bỏ trống")]
        //[Phone( ErrorMessage = "Số điện thoại không hợp lệ")]
        public string Sdt { get; set; }

        private string _diaChi;
        //[Required( ErrorMessage = "Địa chỉ không được bỏ trống")]
        public string DiaChi
        {
            get => _diaChi;
            set => _diaChi = value?.Trim();
        }
        private string _tinNhan;
        public string TinNhan
        {
            get => _tinNhan;
            set => _tinNhan = value?.Trim();
        }
        public string PhuongThucVanChuyen { get; set; }
        public string PhuongThucThanhToan { get; set; }
        public long? ThanhTien { get; set; }
       // public List<CreateChiTietDonHangDto> ChiTietDonHangs { get; set; }
    }

    /*[AutoMapTo(typeof(ChiTietDonHangs))]
    public class CreateChiTietDonHangDto : EntityDto<int>
    {
        //[Range(1, int.MaxValue)]
        public int DonHangId { get; set; }
        //[Range(1, int.MaxValue)]
        public int? SachId { get; set; }
        public int? SoLuong { get; set; }

        //[StringRange(AllowableValues = new string[] { ChiTietDonHangDto.CHUA_XU_LY, ChiTietDonHangDto.DANG_CHUAN_BI_HANG, ChiTietDonHangDto.DANG_GIAO_HANG, ChiTietDonHangDto.DA_NHAN_HANG })]
        public string TrangThai { get; set; }
    }*/
}
