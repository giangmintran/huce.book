﻿using HUCE.Book.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book.QuanLy
{
    public class PagedDonHangRequestDto : PagedQuanLyRequestDtoBase
    {
        public bool? IsDuyet { get; set; }
    }
}
