﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using HUCE.Book.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book.QuanLy
{
    [AutoMapTo(typeof(DonHang))]
    public class UpdateDonHangDto : CreateDonHangDto, IEntityDto<int>
    {
        public int Id { get; set; }
    }

    /*[AutoMapTo(typeof(ChiTietDonHangs))]
    public class UpdateChiTietDonHangDto : CreateChiTietDonHangDto, IEntityDto<int>
    {
        public int Id { get; set; }
    }*/
}
