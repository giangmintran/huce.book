﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HUCE.Book.Entities;
using System.Threading.Tasks;

namespace HUCE.Book.QuanLy
{
    [AutoMapFrom(typeof(DonHang))]
    public class DonHangDto : EntityDto<int>
    {
        public string Ma { get; set; }
        public int? GioHangId { get; set; }
        public int? VoucherId { get; set; }
        public string Sdt { get; set; }
        public string DiaChi { get; set; }
        public string PhuongThucVanChuyen { get; set; }
        public string PhuongThucThanhToan { get; set; }
        public string TinNhan { get; set; }
        public bool IsDuyet { get; set; }
        public long? ThanhTien { get; set; }
        public long? CreatorUserId { get; set; }
        public List<ChiTietGioHangDto> ChiTietGioHangs { get; set; }
    }
    
    /*public class ChiTietDonHangDto : EntityDto<int>
    {
        public int DonHangId { get; set; }
        public int? SachId { get; set; }
        public int? SoLuong { get; set; }
        public string TrangThai { get; set; }
        public const string DANG_GIAO_HANG = "DANG_GIAO_HANG";
        public const string DA_NHAN_HANG = "DA_NHAN_HANG";
        public const string DANG_CHUAN_BI_HANG = "DANG_CHUAN_BI_HANG";
        public const string CHUA_XU_LY = "CHUA_XU_LY";
    }*/
}
