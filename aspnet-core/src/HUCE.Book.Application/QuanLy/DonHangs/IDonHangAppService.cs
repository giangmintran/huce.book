﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book.QuanLy.DonHangs
{
    public interface IDonHangAppService : IAsyncCrudAppService<DonHangDto, int, PagedDonHangRequestDto, CreateDonHangDto, UpdateDonHangDto>
    {
        Task CreateAsync(CreateDonHangDto input);
        Task UpdateAsync(UpdateDonHangDto input);
        Task DeleteAsync(EntityDto<int> input);
        Task<DonHangDto> GetAsync(EntityDto<int> input);
        Task<PagedResultDto<DonHangDto>> GetAllAsync(PagedDonHangRequestDto input);
        //Task<ChiTietDonHangDto> GetChiTietAsync(EntityDto<int> input);
    }
}
