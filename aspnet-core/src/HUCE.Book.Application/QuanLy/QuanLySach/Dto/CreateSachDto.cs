﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using HUCE.Book.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book.QuanLy
{
    [AutoMapTo(typeof(Sach))]
    public class CreateSachDto : EntityDto<int>
    {
        private string _ma;
        [Required(ErrorMessage = "Mã Sách không được bỏ trống")]
        public string Ma
        {
            get => _ma;
            set => _ma = value?.Trim();
        }

        private string _ten;
        [Required(ErrorMessage = "Tên Sách không được bỏ trống")]
        public string Ten
        {
            get => _ten;
            set => _ten = value?.Trim();
        }

        private string _tenEn;
        public string TenEn
        {
            get => _tenEn;
            set => _tenEn = value?.Trim();
        }
        private string _nhaXuatBan;
        [Required(ErrorMessage = "Nhà xuất bản không được bỏ trống")]
        public string NhaXuatBan
        {
            get => _nhaXuatBan;
            set => _nhaXuatBan = value?.Trim();
        }
        private string _tacGia;
        [Required(ErrorMessage = "Tác giả không được bỏ trống")]
        public string TacGia
        {
            get => _tacGia;
            set => _tacGia = value?.Trim();
        }
        public int? SoLuong { get; set; }
        public int? Gia { get; set; }

        private string _moTa;
        public string MoTa
        {
            get => _moTa;
            set => _moTa = value?.Trim();
        }
        public int NhaPhanPhoiId { get; set; }
        public int TheLoaiSachId { get; set; }
        public int KhoId { get; set; }
    }
}
