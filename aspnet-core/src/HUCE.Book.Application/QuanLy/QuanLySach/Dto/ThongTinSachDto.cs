﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using HUCE.Book.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book.QuanLy
{
    [AutoMapFrom(typeof(Sach))]
    public class ThongTinSachDto : EntityDto<int>
    {
        public string Ma { get; set; }
        public string Ten { get; set; }
        public string TenEn { get; set; }
        public string NhaXuatBan { get; set; }
        public string TacGia { get; set; }
        public int? SoLuong { get; set; }
        public long? Gia { get; set; }
        public string MoTa { get; set; }
        public int NhaPhanPhoiId { get; set; }
        public int TheLoaiSachId { get; set; }
        public int KhoId { get; set; }
    }
}
