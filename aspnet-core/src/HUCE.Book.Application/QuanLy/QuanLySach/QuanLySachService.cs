﻿using Abp.Application.Services.Dto;
using Abp.Domain.Uow;
using Abp.UI;
using HUCE.Book.Entities;
using HUCE.Book.EntityFrameworkCore.Repositories;
using HUCE.Book.QuanLy;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUCE.Book
{
    public class QuanLySachService : BookAppServiceBase
    {
        private readonly SachRepository _sachRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public QuanLySachService
            (SachRepository sachRepository,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _sachRepo = sachRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task CreateAsync(CreateSachDto input)
        {
            var unitOfWork = _unitOfWorkManager.Begin(); //Tạo UnitOfWork

            if (_sachRepo.GetDbQueryTable().Any(d => d.Ma == input.Ma && !d.IsDeleted))
            {
                throw new UserFriendlyException($"Mã Sách: {input.Ma} đã tồn tại");
            }

            _sachRepo.Insert(ObjectMapper.Map<Sach>(input));
            CurrentUnitOfWork.SaveChanges();
            await unitOfWork.CompleteAsync();//Hoàn thành 
        }

        public async Task UpdateAsync(UpdateSachDto input)
        {
            if (_sachRepo.GetDbQueryTable().Any(d => d.Ma == input.Ma && !d.IsDeleted))
            {
                throw new UserFriendlyException($"Mã Sách: {input.Ma} đã tồn tại");
            }
            var unitOfWork = _unitOfWorkManager.Begin();
            _sachRepo.Update(ObjectMapper.Map<Sach>(input));
            await unitOfWork.CompleteAsync();
        }

        public async Task DeleteAsync(EntityDto<int> input)
        {
            var unitOfWork = _unitOfWorkManager.Begin();
            var entity = _sachRepo.GetDbQueryTable().Where(d => !d.IsDeleted).FirstOrDefault(e => e.Id == input.Id);
            if (entity == null)
            {
                throw new UserFriendlyException("Không tìm thấy");
            }
            entity.IsDeleted = true;
            CurrentUnitOfWork.SaveChanges();
            await unitOfWork.CompleteAsync();
        }

        public async Task<ThongTinSachDto> GetAsync(EntityDto<int> input)
        {
            var entity = _sachRepo.FirstOrDefault(e => e.Id == input.Id && !e.IsDeleted);
            if (entity == null)
            {
                throw new UserFriendlyException("Không tìm thấy nhà phân phối");
            };
            return ObjectMapper.Map<ThongTinSachDto>(entity);
        }

        public async Task<PagedResultDto<ThongTinSachDto>> GetAllAsync(PagedQuanLyRequestDtoBase input)
        {
            var result = new PagedResultDto<ThongTinSachDto>();
            var query = _sachRepo.GetDbQueryTable().AsQueryable().Where(p => !p.IsDeleted);

            if (!string.IsNullOrEmpty(input.Keyword))
            {
                query = query.Where(o => o.Ten.Contains(input.Keyword)
                                    || o.Ten.Contains(input.Keyword));
            }

            var items = await query.OrderByDescending(o => o.Id)
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount)
                .ToListAsync();
            var Sachs = ObjectMapper.Map<List<ThongTinSachDto>>(items);

            result.Items = Sachs;
            result.TotalCount = await query.CountAsync();
            return result;
        }

        public async Task<List<ThongTinSachDto>> GetAllList()
        {
            var ds = await _sachRepo.GetAllListAsync();
            var result = ObjectMapper.Map<List<ThongTinSachDto>>(ds);
            return result;
        }
    }
}
