﻿using Abp.Application.Services;
using HUCE.Book.MultiTenancy.Dto;

namespace HUCE.Book.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

