﻿using System.Threading.Tasks;
using Abp.Application.Services;
using HUCE.Book.Authorization.Accounts.Dto;

namespace HUCE.Book.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
