using System.ComponentModel.DataAnnotations;

namespace HUCE.Book.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}