﻿using Abp.AspNetCore;
using Abp.AspNetCore.TestBase;
using Abp.Modules;
using Abp.Reflection.Extensions;
using HUCE.Book.EntityFrameworkCore;
using HUCE.Book.Web.Startup;
using Microsoft.AspNetCore.Mvc.ApplicationParts;

namespace HUCE.Book.Web.Tests
{
    [DependsOn(
        typeof(BookWebMvcModule),
        typeof(AbpAspNetCoreTestBaseModule)
    )]
    public class BookWebTestModule : AbpModule
    {
        public BookWebTestModule(BookEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbContextRegistration = true;
        } 
        
        public override void PreInitialize()
        {
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(BookWebTestModule).GetAssembly());
        }
        
        public override void PostInitialize()
        {
            IocManager.Resolve<ApplicationPartManager>()
                .AddApplicationPartsIfNotAddedBefore(typeof(BookWebMvcModule).Assembly);
        }
    }
}