﻿using System.Threading.Tasks;
using HUCE.Book.Models.TokenAuth;
using HUCE.Book.Web.Controllers;
using Shouldly;
using Xunit;

namespace HUCE.Book.Web.Tests.Controllers
{
    public class HomeController_Tests: BookWebTestBase
    {
        [Fact]
        public async Task Index_Test()
        {
            await AuthenticateAsync(null, new AuthenticateModel
            {
                UserNameOrEmailAddress = "admin",
                Password = "123qwe"
            });

            //Act
            var response = await GetResponseAsStringAsync(
                GetUrl<HomeController>(nameof(HomeController.Index))
            );

            //Assert
            response.ShouldNotBeNullOrEmpty();
        }
    }
}